vesper2dayone
=============

[Vesper](http://vesperapp.co) was a fantastic note-taking app for iOS
devices. I have taken many notes in Vesper. It has been shutdown.
Fortunately, it has a note export feature.

[Day One](http://dayoneapp.com) is a journaling and note taking app for
both macOS and iOS. It supports tagging and photos, much as Vesper did.
So, it's a good candidate for taking over Vesper's note taking and journaling
duties.

Day One has a nice command line utility that is very convenient for
creating notes programmatically. It's a great vehicle for
importing my Vesper notes into the Day One app.

Day One for the Mac syncs with Day One for iOS. So, I'll be
able to take notes on all my Apple Devices.

Also, Day One has a very nice export capability. So, if I'm again caught
with a bunch of notes in an app that has to shut down (I have no
foreknowledge about any future shutdown of Day One; for all I know, it's
very solid) I'll have a way to move my notes and my wife's journal to
yet another platform.

Motivation
----------

I convinced my wife that Vesper should be the app that she should use
for her daily journaling. "Vesper is backed by some really
great guys in the Apple community," I told her, "it'll be around for a
long time to come."

[Due](http://inessential.com/2016/08/21/last_vesper_update_sync_shutting_down)
[to](http://inessential.com/2016/08/21/more_notes_on_vesper)
[well-documented](http://inessential.com/2016/08/25/why_vesper_didnt_start_as_a_web_app)
[reasons](http://daringfireball.net/2016/08/vesper_adieu),
the guys behind Vesper have shut it down. Now I need to export my
wife's journal entries out of Vesper and into something else.

Pre-reqs
--------

You'll need:

* The Day One app installed on your Mac: [http://dayoneapp.com](http://dayoneapp.com) (make sure you've launched the Day One app at least once)
* The Day One app installed on your iOS device (you still want the mobile note-taking experience that Vesper gave ya, right?)
* The `dayone` CLI program installed on your Mac: [http://dayoneapp.com/downloads/dayone-cli.pkg](http://dayoneapp.com/downloads/dayone-cli.pkg) 
* exported Vesper text files somewhere on a local filesystem on your Mac
* Python 3.5 or newer (requires `run()` from the *subprocess* module, new in Python 3.5)

\#tags
------

One of Vesper's seminal features was its lightweight, pervasive tagging system.
Day One includes a form of note tagging as well: it recognizes
hashtags (#tags) in notes and converts them into tags in its inbuilt tagging system.

For now, hashtags will be converted to Day One tags as you edit notes that you've
imported. There doesn't seem to be a way to convince Day One to automatically convert all
existing hashtags in imported notes into Day One tags. However, unless dealing with a
gargantuan number of notes, the conversion process — though manual — is straightforward.
Once all notes are imported into the macOS Day One app, you'll quickly edit each note. Sending
each note through the editing process will cause Day One to detect the hashtags in each note.

* Select the last note in the list.
* Press ⌘E.
* Press ⌘E again.
* Press the up arrow key.
* Repeat until you've processed every entry in the list.

Photos
------

Any photos exported by Vesper will imported into Day One and associated with the correct notes.
All your notes' photos should be preserved during the import into Day One.

Archived Notes
--------------

Vesper supported the notion of archived notes. Archived notes were
removed from the main list of notes, but not truly deleted. Day One
doesn't have any notion of archived notes. So, only *active* notes
from Vesper are imported into Day One.

Python: An Aside
----------------

No version of macOS ships with Python 3.5 installed by default. You'll
need to install it. A straightforward way to install a recent version of Python
is to download the installer from the [official Python downloads page](https://www.python.org/downloads/).

A Primer
--------

* Export your notes from Vesper.
    * Tap the **<** icon in the upper right of the Vesper UI.
    * In the left-side menu that appears, choose **Export**.
    * Follow the on-screen directions.
* Import your notes into Day One.
    * Download the most recent version of `vesper2dayone` from Downloads link in the menu to the right.
        * Choose the *Downloads* link in the left-side menu of the vesper2dayone Bitbucket page.
        * Click the *Tags* tab.
        * Download the most recent release.
    * In the Finder, double-click the file you just downloaded.
    * Open the just-uncompressed **vesper2dayone** directory.
    * Select the `vesper2dayone.py` file by clicking on it.
    * Choose **Copy "vesper2dayone.py"** from the **Edit** menu.
    * Launch *Terminal.app* on your Mac.
    * Choose **Paste** from the **Edit** menu.
    * Go back to the Finder.
    * Open a Finder window containing the *Vesper Export ƒ* exported notes directory.
    * Select the exported Vesper notes directory by clicking it.
    * Choose **Copy "Vesper Export ƒ"** from the **Edit** menu.
    * Go back to your Terminal.app window.
    * Choose **Paste** from the **Edit** menu.
    * You should now have a Vesper command in your terminal that looks like:
  
    `/path/to/vesper2dayone /path/to/Vesper\ Export\ ƒ`
  
    * Press the *Return* key on your keyboard. This will start the note import process.
    * Once `vesper2dayone` completes, open the Day One app and make sure the imported notes look as you expect.
