#!/usr/bin/env python3
# -*- coding: utf8 -*-

#   Copyright 2016 Geoff Cleary
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

from datetime import datetime,timedelta
import errno
import os
import shlex
import shutil
import subprocess
import sys
import time

# globals
day_one_app = '/Applications/Day One.app'
day_one_CLI = 'dayone'


def locateDayOne():
    """Make sure both the Day One app and the accompanying CLI program are installed"""
    global day_one_app
    global day_one_CLI

    # Is the Day One app installed?

    day_one_app_exists = os.access( day_one_app, os.F_OK )

    if not day_one_app_exists:
        raise FileNotFoundError( errno.ENOENT, 'Purchase it from the Mac App Store (https://geo.itunes.apple.com/us/app/day-one-2-journal-+-notes/id1055511498?mt=12&at=10lnHI&ct=dayoneappcom)', day_one_app )

    # Is the Day One CLI program installed?

    try:
        day_one_CLI_path = shutil.which( day_one_CLI )    # python ≥ 3.3
    except AttributeError:
        from distutils.spawn import find_executable
        day_one_CLI_path = find_executable( day_one_CLI ) # python < 3.3

    if day_one_CLI_path is None:
        raise FileNotFoundError( errno.ENOENT, 'Download it from http://dayoneapp.com/downloads/dayone-cli.pkg and install it.', day_one_CLI )

    return day_one_CLI_path


def enableDayOneCLI():
    """Enable the dayone CLI to use the Day One.app (version 2) journal file. This'll break usage of classic Day One.app."""

    HOME = os.getenv('HOME')
    plist_file   = '"{}/Library/Group Containers/5U8NS4GX82.dayoneapp/Data/Preferences/dayone.plist"'.format(HOME)
    journal_file = '"{}/Library/Group Containers/5U8NS4GX82.dayoneapp2/Data/Auto Import/Default Journal.dayone"'.format(HOME)
    plist_key    = 'JournalPackageURL'
    
    try:
        plist_value = subprocess.check_output( shlex.split('defaults read {} {}'.format(plist_file, plist_key)),
                                               stderr=subprocess.STDOUT,
                                               universal_newlines=True )
        if plist_value.rstrip() == journal_file.strip('"'):
            # The dayone CLI is already enabled
            return
    except subprocess.CalledProcessError:
        pass

    print( '\n\t:: WARNING ::\n' +
           '\tThe dayone CLI program is installed, but not enabled.\n' +
           '\tEnabling the CLI program will break usage of the classic Day One app.\n\n' +
           '\tContinue onward only if you use version 2 of the Day One app.\n\n' +
           '\tContinue? (Y/n) ', end='' )

    try:
        user_response = raw_input() # python2
    except NameError:
        user_response = input()     # python3

    if not user_response.startswith('Y'):
        # User said no
        print( '\n', end='' )
        sys.exit(os.EX_OK)

    # Enable the dayone CLI program

    try:
        subprocess.check_output( shlex.split('defaults write {} {} {}'.format(plist_file,
                                                                              plist_key,
                                                                              journal_file)),
                                 stderr=subprocess.STDOUT )
        print( '\n\tEnabled the dayone CLI program.\n')
    except subprocess.CalledProcessError as E:
        print( 'Error enabling dayone CLI: {} (rc={})'.format(E.output, E.returncode) )
        sys.exit(os.EX_CONFIG)
# enableDayOneCLI


def getListOfVesperNoteFilenames(path):
    dir_objs = os.scandir(path)
    return [obj.name for obj in dir_objs if obj.is_file() and not obj.name.startswith('.')]


class tzdatetime(datetime):
    """A datetime subclass that adjusts the date/time by the local timezone's UTC offset beforing stringifying"""
    def __str__(self):
        if time.daylight:
            offset = time.altzone
        else:
            offset = time.timezone
        timezone_offset = timedelta(seconds=offset)
        adj_date_time = self + timezone_offset
        return super(datetime,adj_date_time).__str__()


class VesperUtil:
    """Utility class. Used to store/retrieve singleton instance of the note parser.
       (I know that a singleton class is overkill here. I just used this as an opportunity
       to figure how one might implement a singleton in Python.)"""
    class __VesperNoteParser:
        """Singleton parser class for parsing exported Vesper note text files."""
        def __init__(self):
            pass

        def parse(self, path):
            with open( path ) as note_file:
                full_note = note_file.read()

            lines = full_note.splitlines( keepends=True )

            # parse the note file
            # this is a pretty brain-dead parser. In particular, it doesn't
            # handle some cases which might be valid in a few specific Vesper
            # note files. For example, the creation date line begins with a
            # specific tag that could potentially also occur at the beginning
            # of a line of note text. This parser doesn't try very hard to
            # handle such a case.
            title = None
            note_text = ''
            picture_filename = None
            for line in lines:
                if title is None:
                    title = line.strip()
                elif line.startswith('Tags:'):
                    # split off the actual tags into a list
                    # and clean up any whitespace
                    tag_list = line.split( ':' )[1].split( ',' )
                    tag_list = [tag.strip().replace(' ','_') for tag in tag_list]
                    if tag_list[0] == '':
                        tag_list = []
                elif line.startswith( 'Created:' ):
                    creation_date_str = line.partition( ':' )[2].strip()    #Mo No, Yr, Time  AM/PM
                    try:
                        creation_date = tzdatetime.strptime( creation_date_str, '%b %d, %Y, %I:%M %p' )
                    except ValueError:
                        creation_date = tzdatetime.strptime( creation_date_str, '%d %b %Y, %H:%M' )
                elif line.startswith( 'Modified:' ):
                    pass
                elif line.startswith( 'Picture:' ):
                    picture_filename = line.partition( ':' )[2].strip()
                else: # actual note text
                    note_text += line
            #for

            return ( title,
                     note_text,
                     tag_list,
                     creation_date,
                     picture_filename )
        #parse()
    #__VesperNoteParser


    vesper_parser = None

    @staticmethod
    def getParser():
        """Return singleton instance of Vesper note parser."""
        if VesperUtil.vesper_parser is None:
            VesperUtil.vesper_parser = VesperUtil.__VesperNoteParser()
        return VesperUtil.vesper_parser
    #getParser
#VesperUtil


class VesperNote:

    def __init__( self, path, note_name ):
        self.path = path + os.sep + note_name
        vesper_note_parser = VesperUtil.getParser()
        (self.title,self.note_text,self.tags,self.creation_date,picture_pathname) = vesper_note_parser.parse( self.path )

        # find a corresponding photo, if there is one

        if picture_pathname is not None:
            self.picture_pathname = path + os.sep + 'Pictures' + os.sep + picture_pathname
    #__init__

    def exportToDayOne(self):
        global day_one_CLI

        # Convert tags into hashtags, which Day One will automatically recognize as tags

        tag_str = self._tagsToHashtags()

        # Export to Day One with the dayone CLI program

        #if self.picture_pathname is not None:
        try:
            picture_str = ' --photo-file="{}"'.format(self.picture_pathname)
        except AttributeError:
            picture_str = ''

        command_str = '{} --date="{}"{} new'.format(day_one_CLI, self.creation_date, picture_str)
        try:
            rc = subprocess.run( shlex.split(command_str),
                                 input='\n'.join((self.title,self.note_text,tag_str)),
                                 universal_newlines=True,
                                 stdout=subprocess.PIPE,
                                 check=True )
        except subprocess.CalledProcessError as E:
            print( 'Couldn\'t import Vesper note from {} due to dayone error: {} (rc={})\npath={}'.format(self.creation_date,
                                                                                                          E.output,
                                                                                                          E.returncode,
                                                                                                          self.path) )
    #exportToDayOne

    def _tagsToHashtags(self):
        return ' '.join(['#' + tag for tag in self.tags])
    #__tagsToHashtags
#VesperNote


def getUserSuppliedPath():
    """Grab the first command line arg and ensure it is a valid path name"""
    try:
        path = sys.argv[1].rstrip( os.sep )
    except IndexError:
        print( '\n\tProvide the path name that holds the notes exported by Vesper\n\tUsage: vesper2dayone.py <path>\n' )
        sys.exit(os.EX_USAGE)

    user_path_exists = os.access( path, os.F_OK )
    if not user_path_exists:
        print( '\n\tThe supplied path doesn\'t exist\n\t({})\n'.format(path) )
        sys.exit(os.EX_DATAERR)

    return path
#getUserSuppliedPath


def checkPythonVersion():
    info = sys.version_info
    (major, minor, micro) = (info.major, info.minor, info.micro)

    if major == 3 and minor >= 5:
        return

    sys.exit( '\n\tYou must be using version 3.5 of Python or newer. ' +
              'You\'re using version {}.{}.{}\n'.format(major, minor, micro) +
              '\tVisit https://www.python.org/downloads/ to get a newer version Python.\n' )
#checkPythonVersion


def runTheApp():

    checkPythonVersion()

    # Ensure that Day One.app and the dayone CLI are installed

    try:
        day_one_CLI = locateDayOne() 
    except FileNotFoundError as E:
        sys.exit( '\n\tCouldn\'t find {}: {}\n'.format(E.filename, E.strerror) )

    # Enable the dayone CLI

    enableDayOneCLI()

    # Ensure supplied path is rational

    path = getUserSuppliedPath()

    # Import the notes into Day One

    path += os.sep + 'Active Notes'

    note_list = getListOfVesperNoteFilenames(path)
    num_notes = len(note_list)
    note_num = 0
    backspaces = '\b'*13

    for note_file in note_list:
        note = VesperNote( path, note_file )
        note.exportToDayOne()
        note_num+=1
        print( '{}{}% complete'.format(backspaces, int(note_num*100/num_notes)), end='', flush=True )

    print('')
#runTheApp


if __name__ == "__main__":
    runTheApp()
